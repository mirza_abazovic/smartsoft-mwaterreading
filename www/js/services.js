angular.module('starter.services', [])

.factory('Readings', function($ionicPopup,$rootScope) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var streets = {
    totalReadings: 22,
    doneReadings:12,
    todoReadings:10,
    data:
  [{
    streetId: 1,
    streetName: 'Grbavička',
    totalReadings: 15,
    doneReadings:10,
      todoReadings:5,
    readings : [
      {serialNumber:"asdas",user:"Mirza",oldReading:15,newReading:0,isReset:false,isRead:false,note:""},
    {serialNumber:"123123",user:"Pero",oldReading:78,newReading:0,isReset:false,isRead:false,note:""}
    ]  
  }, 
  {
    streetId: 2,
    streetName: 'Pionirska',
    totalReadings: 5,
    doneReadings:1,
    todoReadings:4,
    readings : [
     {serialNumber:"asdas",user:"Mirza",oldReading:15,newReading:0,isReset:false,isRead:false,note:""},
    {serialNumber:"123123",user:"Pero",oldReading:78,newReading:0,isReset:false,isRead:false,note:""},
      {serialNumber:"asdas",user:"Mirza",oldReading:15,newReading:0,isReset:false,isRead:false,note:""},
    {serialNumber:"123123",user:"Pero",oldReading:78,newReading:0,isReset:false,isRead:false,note:""},
      {serialNumber:"asdas",user:"Mirza",oldReading:15,newReading:0,isReset:false,isRead:false,note:""},
    {serialNumber:"123123",user:"Pero",oldReading:78,newReading:0,isReset:false,isRead:false,note:""}
    ]  
  }, 
  {
    streetId: 3,
    streetName: 'Azize Šačirbegović',
    totalReadings :2,
    doneReadings:1,
    todoReadings:1,
    readings :[
    {serialNumber:"asdas",user:"Mirza",oldReading:15,newReading:0,isReset:false,isRead:false,note:""},
    {serialNumber:"123123",user:"Pero",oldReading:78,newReading:0,isReset:false,isRead:false,note:""}
    ]  
  }]
};

  return {
    allStreets: function() {
      //return streets;
      var streetsFromDb = {};
      streetsFromDb.data=[];
      var db = new PouchDB('readings');
      db.allDocs({
  include_docs: true,
}).then(function (result) {
  var res = result.rows;
  var grouped = _.groupBy(res, function(obj){ return obj.doc.reading.sifraUlice; });
  var counts = _.countBy(res, function(obj){ return obj.doc.reading.nazivUlice; });
  for(var key in grouped) {
    var streetArray = grouped[key] 
   //console.log(grouped[key]);
   var streetId=streetArray[0].doc.reading.sifraUlice;
   var streetName=streetArray[0].doc.reading.nazivUlice;
   var readings = [];
   for (var i = 0; i < streetArray.length; i++) {
          var reading = streetArray[i].doc.reading;
          //console.log(streetArray[i].doc.reading);
          readings.push({
            objectId:reading.objectId,
            serialNumber:reading.serijskiBroj,
            user:reading.nazivKorisnika,
            oldReading:reading.zadnjeStanjeBrojila,
            newReading:reading.novoOcitanje,
            isReset:reading.isReset,
            isRead:reading.isRead,
            note:reading.note});
   }
    streetsFromDb.data.push({ 
      streetId:streetId,
      streetName:streetName,
      readings:readings});
 }
 return streetsFromDb;
  }).catch(function (err) {
  console.log(err);
});
    },
    getStatus: function(ulicaId) {
      if(typeof ulicaId === "undefined"){
             return {totalReadings:streets.totalReadings,doneReadings:streets.doneReadings,todoReadings:streets.todoReadings};
    
      }
      else {
        var street ={};
        for (var i = 0; i < streets.data.length; i++) {
        if (streets.data[i].streetId === parseInt(ulicaId)) {
          street  = streets.data[i];
        }
      }
        return {totalReadings:street.totalReadings,doneReadings:street.doneReadings,todoReadings:street.todoReadings};
      }

   
    },


    getStreet: function(ulicaId) {
      for (var i = 0; i < streets.data.length; i++) {
        if (streets.data[i].streetId === parseInt(ulicaId)) {
          return streets.data[i];
        }
      }
      return null;
    }
  };
});
