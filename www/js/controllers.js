angular.module('starter.controllers', [])


.controller('StreetsController', function($scope, $state, $rootScope,$ionicLoading) {

    if (!$rootScope.isLoggedIn) {
        $state.go('welcome');
    }

    $rootScope.refreshStreets = function(){
      $ionicLoading.show();
      var doneReadings = 0;
      var streetsFromDb = {};
      streetsFromDb.data=[];
      var db = new PouchDB('readings',{auto_compaction: true});
      db.allDocs({include_docs: true,})
      .then(function (result) {
        var res = result.rows;
        streetsFromDb.totalReadings = res.length;
        var grouped = _.groupBy(res, function(obj){ return obj.doc.sifraUlice; });
        //var counts = _.countBy(res, function(obj){ return obj.doc.nazivUlice; });
        for(var key in grouped) {
            var streetArray = grouped[key] 
            var totalReadingsStreet = streetArray.length;
            var doneReadingsStreet = 0;
            var todoReadingsStreet = 0;
            var streetId=streetArray[0].doc.sifraUlice;
            var streetName=streetArray[0].doc.nazivUlice;
            var readings = [];
            for (var i = 0; i < streetArray.length; i++) {
                    var reading = streetArray[i].doc;
                    if(reading.isRead){
                        doneReadingsStreet++;
                    }
                    /*
                    readings.push({
                        objectId:reading.objectId,
                        serialNumber:reading.serijskiBroj,
                        user:reading.nazivKorisnika,
                        oldReading:reading.zadnjeStanjeBrojila,
                        novoOcitanje:reading.novoOcitanje,
                        isReset:reading.isReset,
                        isRead:reading.isRead,
                        note:reading.note});
                    */
                    }
                    streetsFromDb.data.push({ 
                        streetId:streetId,
                        streetName:streetName,
                         totalReadings: totalReadingsStreet,
                         doneReadings: doneReadingsStreet,
                         todoReadings:totalReadingsStreet-doneReadingsStreet
                        //readings:readings
                    });
                    doneReadings = doneReadings + doneReadingsStreet
                }
                streetsFromDb.doneReadings = doneReadings;

                streetsFromDb.todoReadings = streetsFromDb.totalReadings - streetsFromDb.doneReadings;

                $scope.streets = streetsFromDb;
                $scope.$apply();
                $ionicLoading.hide();
            }).catch(function (err) {
                console.log(err);
            });
    }

  $rootScope.refreshStreets();

    $scope.clearSearch = function(){
        $scope.searchText="";
    }

})

.controller('StreetDetailController', function($scope, $rootScope,$stateParams,$ionicLoading) {
    if (!$rootScope.isLoggedIn) {
        $state.go('welcome');
    }
   $scope.street = {};
   $scope.save = function(reading){
            var db = new PouchDB('readings',{auto_compaction: true});
            //console.log(reading);
            db.get(reading.objectId).then(function(doc) {
                                          var vijemeUnosaStr = new Date(new Date().getTime() + (-new Date().getTimezoneOffset()*60000)).toISOString();
                                          doc._id = reading.objectId;
                                          doc.vrijemeUnosa =  vijemeUnosaStr;
                                          doc.novoOcitanje = reading.novoOcitanje;
                                          doc.isRead = reading.isRead;
                                          doc.isReset = reading.isReset;                              
                                          doc.note = reading.note;
                                          doc._id = reading.objectId;
                                          

              return db.put(doc);
            }).then(function(response) {
               //console.log('after change: '+JSON.stringify(response));
                $scope.refresh();
            }).catch(function (err) {
              console.log(err);
            });
           

   };

$scope.refresh = function(){
 var db = new PouchDB('readings',{auto_compaction: true});
    //PouchDB.debug.enable('*');
        //$ionicLoading.show();
   
     db.allDocs({include_docs: true,})
     .then(function (result) {
        var res = result.rows;
        var grouped = _.groupBy(res, function(obj){ return obj.doc.sifraUlice; });
        for(var key in grouped) {
            if(key===$stateParams.streetId)
            {
                var streetArray = grouped[key] 
                var totalReadingsStreet = streetArray.length;
                var doneReadingsStreet = 0;
                var todoReadingsStreet = 0;
                var streetName=streetArray[0].doc.nazivUlice;
                var streetId=streetArray[0].doc.sifraUlice;
                var readings = [];
                for (var i = 0; i < streetArray.length; i++) {
                        var reading = streetArray[i].doc;
                        if(reading.isRead){
                            doneReadingsStreet++;
                        }
                        readings.push({
                            _id : reading._id,
                            _rev : reading._rev,                            
                            objectId : reading.objectId,
                            serialNumber : reading.serijskiBroj,
                            kucniBroj : reading.kucniBroj,
                            user : reading.nazivKorisnika,
                            oldReading : reading.zadnjeStanjeBrojila,
                            novoOcitanje : reading.novoOcitanje,
                            isReset : reading.isReset,
                            isRead : reading.isRead,
                            note:reading.note
                          });
                    }

                    $scope.street = { 
                         streetId:streetId,
                         streetName:streetName,
                         totalReadings: totalReadingsStreet,
                         doneReadings: doneReadingsStreet,
                         //todoReadings:totalReadingsStreet-doneReadingsStreet,
                         readings:readings
                    };
                $scope.$apply();
                for(var ii=0; ii<$scope.street.readings.length; ii++) {
                    $scope.$watch('street.readings['+ii+']', function(changed) {
                       if(changed.novoOcitanje=="")
                        return;                        
                        if(changed.novoOcitanje>=changed.oldReading){
                            changed.isRead=true;
                        }
                        else {
                             changed.isRead=false;
                        }                     
                }, true);}   
                //$ionicLoading.hide();     
                } 
            }
      }).catch(function (err) {
          console.log(err);
            });

}; //end refresh 




/*
 
*/
  $scope.search = function (item){
    if(typeof $scope.searchText === "undefined")
        return true;
    if (item.user.toLowerCase().indexOf($scope.searchText.toLowerCase())!=-1 || item.serialNumber.toLowerCase().indexOf($scope.searchText.toLowerCase())!=-1) {
            return true;
        }
        return false;
  };

  $scope.refresh();
  
})


.controller('AppController', function($scope, $state, $rootScope, $ionicHistory, $stateParams) {
    if ($stateParams.clear) {
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
    }

    $scope.logout = function() {
        Parse.User.logOut();
        $rootScope.user = null;
        $rootScope.isLoggedIn = false;
        $state.go('welcome', {
            clear: true
        });
    };
})

.controller('WelcomeController', function($scope, $state, $rootScope, $ionicHistory, $stateParams) {
    if ($stateParams.clear) {
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache();
    }

    $scope.login = function() {
        $state.go('app.login');
    };

    $scope.signUp = function() {
        $state.go('app.register');
    };

    if ($rootScope.isLoggedIn) {
        $state.go('app.home');
    }
})

.controller('HomeController', function($scope, $state, $rootScope, $http,$ionicPopup,$ionicLoading) {
    
    if (!$rootScope.isLoggedIn) {
        $state.go('welcome');
    }
    $scope.username = $rootScope.user.username;
    $rootScope.$on('$stateChangeStart', 
function(event, toState, toParams, fromState, fromParams){ 
    if(toState.views.menuContent.controller === "StreetsController"){
        //refresh streets data 
       $rootScope.refreshStreets();
    }
      if(toState.views.menuContent.controller === "HomeController"){
         
        $rootScope.brojPoslanihOcitanja=0;
        $rootScope.brojGresaka=0;
           
    }
})

      $scope.sendReadings = function (){
        $rootScope.brojPoslanihOcitanja=0;
          $rootScope.brojGresaka = 0;
        var db = new PouchDB('readings',{auto_compaction: true});
         db.allDocs({include_docs: true,})
     .then(function (result) {
        var res = result.rows;
        updateParseData(res);
       
      }).catch(function (err) {
          console.log(err);
            });
      };

var updateParseData = function (result) {
    var brojOcitanja = 0 ;
    var sessionToken = $rootScope.user._sessionToken;
    var appKey = "g4ACn6InDF6lS33qqIIHJyOon9D17idsrXBpAWTj";
    var apiKey = "esiQ0ZGyHHGAVDI3ucN2V3FXPksBu14ArY5WnaVs";
    if (result != null && result.length>0) {       
        var Ocitanje = Parse.Object.extend({ className: "Ocitanje" });
        var query = new Parse.Query(Ocitanje);
        var changes = 1;        
       if (!sessionToken) {
            alert("Sesija istekla molimo prijavite se ponovo");
            return;
        }
        for (k = 0; k < result.length; k++) {
            var doc = result[k].doc;
            if(doc.isRead){
                 var url = "https://api.parse.com/1/classes/Ocitanje/" + doc.objectId;
                   var data = { 
                "novoOcitanje": doc.novoOcitanje, 
                "isRead": doc.isRead, 
                "isReset": doc.isReset, 
                "datumOcitanja": doc.vrijemeUnosa,
                "note" : doc.note
                 };
                  $http({
                    method: 'PUT', 
                    url:url,
                    data :data,
                    headers: {
                    "X-Parse-Application-Id": appKey,
                    "X-Parse-REST-API-Key": apiKey,
                    "X-Parse-Session-Token": sessionToken
                    }
                })
                 .success( function( response, status, headers, config )
                 {
                     $rootScope.brojPoslanihOcitanja++;
                     $scope.$apply();
                    //console.log(response);
                 })
                 .error( function( response, status, headers, config)
                 {
                      $rootScope.brojGresaka++;
                     //console.log(response);
                 }); 
            }//if end                          
        }//for end
    }//if end
};




    $scope.getReadings = function (){
        PouchDB.allDbs().then(function (dbs) {
  // dbs is an array of strings, e.g. ['mydb1', 'mydb2']
  if (dbs.indexOf("readings") > -1) {
        //In the array!
         // A confirm dialog
       
       var confirmPopup = $ionicPopup.confirm({
         title: 'Brisanje podataka',
         template: 'Postojeći podaci će biti OBRISANI. Jeste li sigurni da želite preuzeti podatke?'
         /*
         ,buttons: [
              { text: 'NE' },
              {
                text: '<b>DA</b>',
                type: 'button-positive'
              }
            ]*/
       });
       confirmPopup.then(function(res) {
         if(res) {
           //alert('You are sure');
            recreateDb();

         } else {
           //alert('You are not sure');
         }
       });


    } else {
        //Not in the array
        recreateDb();
    }
    }).catch(function (err) {
      // handle err
    });

       
    var  recreateDb = function(){
         var db = new PouchDB('readings',{auto_compaction: true});
            db.destroy().then(function () {
        // database destroyed
         var db = new PouchDB('readings',{auto_compaction: true});
                var Ocitanje = Parse.Object.extend("Ocitanje");
        var query = new Parse.Query(Ocitanje);
        query.limit(1000);
        query.equalTo("username", $rootScope.user.attributes.username);
        query.find({
            success: function (results) {
                if (results.length == 0) {
                    $ionicLoading.hide();
                    
                  $ionicPopup.alert({
                  title: 'Nema podataka',
                  template : 'Nema podataka na serveru'
                });
                }
                else {
                    var res =JSON.parse(JSON.stringify(results));
                    for (var i = 0; i < res.length; i++) {
                        res[i]._id=res[i].objectId;
                    };

                    //PouchDB.debug.enable('*');
                    var db = new PouchDB('readings',{auto_compaction: true});
                    
                    db.bulkDocs(res)
                    .then(function (result) {
                    // handle result
                    $ionicLoading.hide();
                     $ionicPopup.alert({
                        title: 'Podaci snimljeni',
                        template : 'Podaci sa servera su preuzeti'
                        });
                    
                }).catch(function (err) {
                console.log(err);
                $ionicLoading.hide();
                    
                });
                    
                }
            },
            error: function (error) {
                 $ionicLoading.hide();
              $ionicPopup.alert({
                  title: 'Nema podataka',
                  template : 'Došlo je do greške ' + error.code + ' '  + error.message
                });
                
            }
        });

        }).catch(function (err) {
        // error occurred
        })   

    }
          
    

    };
})//controler end

.controller('LoginController', function($scope, $state, $rootScope, $ionicLoading) {
    $scope.user = { };

    $scope.error = {};

    function userHasRole(user, roleName) {
          var queryRole = new Parse.Query(Parse.Role);
          queryRole.equalTo("name", roleName);
          queryRole.equalTo("users", user);
          return queryRole.find().then(function(roles) {
              return roles.length > 0;
          });
        }
    $scope.login = function() {
        $scope.loading = $ionicLoading.show({
            content: 'Logging in',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        var user = $scope.user;
        Parse.User.logIn(user.username, user.password, {
            success: function(user) {
             /*
              userHasRole(currentUser, "admin").then(function(isAdmin) {
                    alert((isAdmin)? "user is admin" : "user is not admin");
              });
*/
                $ionicLoading.hide();
                $rootScope.user = user;
                $rootScope.isLoggedIn = true;
                $state.go('app.home', {
                    clear: true
                });
            },
            error: function(user, err) {
                $ionicLoading.hide();
                // The login failed. Check error to see why.
                if (err.code === 101) {
                    $scope.error.message = 'Prijava korisnika '+$scope.user.username+' nije uspjela. Probajte ponovo';
                } else {
                    $scope.error.message = 'Desila se greška, ' +
                        'pokušajte ponovo.';
                }
                $scope.$apply();
            }
        });
    };//function login END

    $scope.forgot = function() {
        $state.go('app.forgot');
    };
})

.controller('ForgotPasswordController', function($scope, $state, $ionicLoading) {
    $scope.user = {};
    $scope.error = {};
    $scope.state = {
        success: false
    };

    $scope.reset = function() {
        $scope.loading = $ionicLoading.show({
            content: 'Sending',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        Parse.User.requestPasswordReset($scope.user.email, {
            success: function() {
                // TODO: show success
                $ionicLoading.hide();
                $scope.state.success = true;
                $scope.$apply();
            },
            error: function(err) {
                $ionicLoading.hide();
                if (err.code === 125) {
                    $scope.error.message = 'Email adresa ne postoji';
                } else {
                    $scope.error.message = 'Desila se greška, ' +
                        'pokušajte ponovo';
                }
                $scope.$apply();
            }
        });
    };

    $scope.login = function() {
        $state.go('app.login');
    };
})

.controller('RegisterController', function($scope, $state, $ionicLoading, $rootScope) {
    $scope.user = {};
    $scope.error = {};

    $scope.register = function() {

        // TODO: add age verification step

        $scope.loading = $ionicLoading.show({
            content: 'Sending',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
        });

        var user = new Parse.User();
        user.set("username", $scope.user.email);
        user.set("password", $scope.user.password);
        user.set("email", $scope.user.email);

        user.signUp(null, {
            success: function(user) {
                $ionicLoading.hide();
                $rootScope.user = user;
                $rootScope.isLoggedIn = true;
                $state.go('app.home', {
                    clear: true
                });
            },
            error: function(user, error) {
                $ionicLoading.hide();
                if (error.code === 125) {
                    $scope.error.message = 'Molimo upišite ispravan email';
                } else if (error.code === 202) {
                    $scope.error.message = 'Email se već koristi';
                } else {
                    $scope.error.message = error.message;
                }
                $scope.$apply();
            }
        });
    };
})

.controller('MainController', function($scope, $state, $rootScope, $stateParams, $ionicHistory) {
    if ($stateParams.clear) {
        $ionicHistory.clearHistory();
    }

    $scope.rightButtons = [{
        type: 'button-dark',
        content: '<i class="icon ion-navicon"></i>',
        tap: function(e) {
            $scope.sideMenuController.toggleRight();
        }
    }];

    $scope.logout = function() {
        Parse.User.logOut();
        $rootScope.user = null;
        $rootScope.isLoggedIn = false;
        $state.go('welcome', {
            clear: true
        });
    };

    $scope.toggleMenu = function() {
        $scope.sideMenuController.toggleRight();
    };
});
